Miikana Docker Setup for CI
===========================

iThis project contains the recipes to create the docker containers used to run
the CI infrastructure for Miikana project.

There is currently only a Jenkins master docker and Jenkins persistent-store
docker, but the rest are coming.

This project uses docker-compose_ to build and control the containers from the
command line.  I don't know how it will get set up on the production server.

Starting the containers
-----------------------

.. code-block:: sh

    docker-compose build
    docker-compose up -d
    docker-compose ps

Stopping the containers
-----------------------

.. code-block:: sh

    docker-compose kill


.. _docker-compose: https://docs.docker.com/compose/
